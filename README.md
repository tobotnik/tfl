## Running the application

Example:

  java ... owl.tfl.main.TflLineApp -o dir --stop=940GZZLUWLO --line=bakerloo

'-o' sets the destination path and defaults to '.'

## Design

The application polls the API only when the known list of arrivals is completely exhausted.

It sleeps between arrivals.

* Dependencies:
** httpcomponents for calling the TFL API
** jackson for JSON
* ArrivalsConsumer is the main class!
** not implemented: 'further arrivals'
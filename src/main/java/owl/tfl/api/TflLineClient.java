package owl.tfl.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.inject.Inject;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import owl.tfl.model.ArrivalPrediction;

import java.io.IOException;
import java.util.List;

/**
 * Created by codechallenge on 16/04/2018.
 */
public class TflLineClient {
    private final ObjectMapper om;
    private final CloseableHttpClient httpClient;

    @Inject
    TflLineClient(ObjectMapper om, CloseableHttpClient httpClient) {

        this.om = om;
        this.httpClient = httpClient;
    }

    public static void main(String[] args) throws IOException {
        //demo
        TflLineClient inst = new TflLineClient(new ObjectMapper()
                .registerModule(new JavaTimeModule()),
                HttpClients.createDefault());

        List<ArrivalPrediction> arrivals = inst.lineArrivals("bakerloo", "940GZZLUWLO", "inbound");
        arrivals.forEach(System.out::println);
    }

    // eg https://api.tfl.gov.uk/Line/bakerloo/Arrivals/940GZZLUWLO?direction=inbound
    public List<ArrivalPrediction> lineArrivals(String line, String stop, String direction) throws IOException {
        HttpGet httpGet = new HttpGet(String.format("https://api.tfl.gov.uk/Line/%s/Arrivals/%s?direction=%s",
                line, stop, direction));
        HttpEntity entity;
        try (CloseableHttpResponse response1 = httpClient.execute(httpGet)) {
            entity = response1.getEntity();
            return om.readValue(entity.getContent(), om.getTypeFactory().constructCollectionType(List.class, ArrivalPrediction.class));
        }
    }
}

package owl.tfl.main;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.time.Clock;

/**
 * Created by codechallenge on 16/04/2018.
 */
public class TflModule extends AbstractModule {


    @Option(name = "--stop", required = true, usage = "stop point id", metaVar = "STOP")
    private String stop;
    @Option(name = "--line", required = true, usage = "line id", metaVar = "LINE")
    private String line;
    @Option(name = "-o", usage = "output dir for side effects", metaVar = "OUTPUT")
    private File out = new File(".");

    public TflModule(String[] args) throws CmdLineException {

        CmdLineParser commandLineParser = new CmdLineParser(this);
        commandLineParser.parseArgument(args);
    }

    @Override
    protected void configure() {
        bind(String.class).annotatedWith(Names.named("LINE")).toInstance(line);
        bind(String.class).annotatedWith(Names.named("STOP")).toInstance(stop);

        bind(Clock.class).toProvider(Clock::systemDefaultZone);
    }

    @Provides
    @Singleton
    public ObjectMapper objectMapper() {
        return new ObjectMapper()
                .registerModule(new JavaTimeModule());

    }


    @Provides
    @Singleton
    public IntegrationPoint integrationPoint(Clock clock) {
        return new IntegrationPoint(clock, out);

    }

    @Provides
    @Singleton
    public CloseableHttpClient httpClient() {
        return HttpClients.createDefault();

    }
}

package owl.tfl.main;

import com.google.inject.Inject;
import owl.tfl.model.ArrivalPrediction;

import java.io.IOException;
import java.time.Clock;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Math.min;

/**
 * Created by codechallenge on 16/04/2018.
 */
public class ArrivalsConsumer {
    private final IntegrationPoint integrationPoint;
    private final Clock clock;

    @Inject
    public ArrivalsConsumer(IntegrationPoint integrationPoint, Clock clock) {
        this.integrationPoint = integrationPoint;
        this.clock = clock;
    }

    /**
     * blocks until all projected arrivals have occurred
     *
     * @param arrivals
     */
    public void consumeAll(List<ArrivalPrediction> arrivals) throws IOException, InterruptedException {

        Instant now = clock.instant();
        // ignore any that arrived in the past
        arrivals = arrivals.stream().filter(a -> now.isBefore(a.getExpectedArrival().toInstant()))
                .collect(Collectors.toList());
        // sort by time
        arrivals.sort(new SORT_ARRIVALS());
        for (int i = 0; i < arrivals.size(); i++) {
            ArrivalPrediction next = arrivals.get(i);
            List<ArrivalPrediction> arrivalsAfter = arrivals.subList(
                    min(arrivals.size(), i + 1),
                    min(arrivals.size(), i + 3));
            System.out.println("next: " + next);
            integrationPoint.sendArrival(format(next));
            integrationPoint.sendFollowingArrivals(formatFollowing(arrivalsAfter));
            sleepFor(next);
        }
    }

    //TODO factor out blocking here
    private void sleepFor(ArrivalPrediction next) throws InterruptedException {
        long millis = clock.instant().until(next.getExpectedArrival().toInstant(), ChronoUnit.MILLIS);
        System.out.println("sleeping for " + millis);
        Thread.sleep(millis);
    }

    private String format(ArrivalPrediction arrival) {
        return String.format("%s: to %s",
                arrival.getExpectedArrival().format(DateTimeFormatter.ISO_TIME), arrival.getDestinationName());
    }

    private List<String> formatFollowing(List<ArrivalPrediction> arrivalsAfter) {
        return arrivalsAfter.stream().map(this::format).collect(Collectors.toList());
    }

    private class SORT_ARRIVALS implements Comparator<ArrivalPrediction> {
        @Override
        public int compare(ArrivalPrediction o1, ArrivalPrediction o2) {
            return o1.getExpectedArrival().compareTo(o2.getExpectedArrival());
        }
    }
}

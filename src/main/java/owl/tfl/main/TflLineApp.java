package owl.tfl.main;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import owl.tfl.api.TflLineClient;
import owl.tfl.model.ArrivalPrediction;

import java.util.List;

/**
 * Created by codechallenge on 16/04/2018.
 */
public class TflLineApp {

    private final String stop;
    private final String line;
    private final TflLineClient client;
    private ArrivalsConsumer arrivalsConsumer;

    @Inject
    public TflLineApp(@Named("STOP") String stop, @Named("LINE") String line,
                      TflLineClient client, ArrivalsConsumer arrivalsConsumer) {
        this.stop = stop;
        this.line = line;
        this.client = client;
        this.arrivalsConsumer = arrivalsConsumer;
    }

    public static void main(String[] args) throws Exception {

        try {
            TflLineApp app = Guice.createInjector(new TflModule(args))
                    .getInstance(TflLineApp.class);
            app.runMain();
        } catch (Exception e) {
            System.err.println("error" + e);
            System.exit(3);
        }
        System.exit(0);
    }


    private void runMain() throws Exception {
        while (true) {
            List<ArrivalPrediction> arrivals = client.lineArrivals(line, stop, "inbound");
            arrivalsConsumer.consumeAll(arrivals);
        }
        // read state
        // dump state
    }
}

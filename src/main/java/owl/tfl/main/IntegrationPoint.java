package owl.tfl.main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.time.Clock;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by codechallenge on 16/04/2018.
 */
class IntegrationPoint {
    private final Clock clock;
    private final Path dir;

    public IntegrationPoint(Clock clock, File dir) {
        this.clock = clock;
        this.dir = dir.toPath();
    }

    public void sendArrival(String text) throws IOException {
        writeFile("arrival", text);
    }

    private void writeFile(String prefix, String text) throws IOException {
        String fName = DateTimeFormatter.ISO_INSTANT.format(clock.instant());
        File dest = dir.resolve(prefix + "." + fName).toFile();
        try (FileOutputStream f = new FileOutputStream(dest);
             PrintWriter p = new PrintWriter(f)
        ) {
            p.write(text);
            p.write("\n");
        }
    }

    public void sendFollowingArrivals(List<String> texts) throws IOException {
        writeFile("nextarrivals", texts.stream().collect(Collectors.joining("\n"))
        );
    }
}

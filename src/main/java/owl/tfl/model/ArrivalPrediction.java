package owl.tfl.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.ZonedDateTime;

/**
 * Created by codechallenge on 16/04/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArrivalPrediction {

    @JsonProperty
    final ZonedDateTime expectedArrival;
    @JsonProperty
    final String destinationName;

    @JsonCreator()
    public ArrivalPrediction(
            @JsonProperty("expectedArrival") ZonedDateTime expectedArrival,
            @JsonProperty("destinationName") String destinationName) {
        this.expectedArrival = expectedArrival;
        this.destinationName = destinationName;

    }

    public ZonedDateTime getExpectedArrival() {
        return expectedArrival;
    }

    public String getDestinationName() {
        return destinationName;
    }

    @Override
    public String toString() {
        return "ArrivalPrediction{" +
                "expectedArrival=" + expectedArrival +
                ", destinationName='" + destinationName + '\'' +
                '}';
    }
}

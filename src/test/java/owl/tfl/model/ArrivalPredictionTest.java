package owl.tfl.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Test;

import java.io.IOException;
import java.time.Instant;

import static org.junit.Assert.assertEquals;

/**
 * Created by codechallenge on 16/04/2018.
 */
public class ArrivalPredictionTest {
    @Test
    public void testDeser() throws IOException {
        ArrivalPrediction a = getObjectMapper().readValue("  {\n" +
                "    \"$type\": \"Tfl.Api.Presentation.Entities.Prediction, Tfl.Api.Presentation.Entities\",\n" +
                "    \"id\": \"253952329\",\n" +
                "    \"operationType\": 1,\n" +
                "    \"vehicleId\": \"205\",\n" +
                "    \"naptanId\": \"940GZZLUWLO\",\n" +
                "    \"stationName\": \"Waterloo Underground Station\",\n" +
                "    \"lineId\": \"bakerloo\",\n" +
                "    \"lineName\": \"Bakerloo\",\n" +
                "    \"platformName\": \"Southbound - Platform 4\",\n" +
                "    \"direction\": \"inbound\",\n" +
                "    \"bearing\": \"\",\n" +
                "    \"destinationNaptanId\": \"940GZZLUEAC\",\n" +
                "    \"destinationName\": \"Elephant & Castle Underground Station\",\n" +
                "    \"timestamp\": \"2018-04-16T10:07:02Z\",\n" +
                "    \"timeToStation\": 889,\n" +
                "    \"currentLocation\": \"At Paddington Platform 4\",\n" +
                "    \"towards\": \"Elephant and Castle\",\n" +
                "    \"expectedArrival\": \"1970-01-01T00:08:20Z\",\n" +
                "    \"timeToLive\": \"2018-04-16T10:21:51Z\",\n" +
                "    \"modeName\": \"tube\",\n" +
                "    \"timing\": {\n" +
                "      \"$type\": \"Tfl.Api.Presentation.Entities.PredictionTiming, Tfl.Api.Presentation.Entities\",\n" +
                "      \"countdownServerAdjustment\": \"00:00:00\",\n" +
                "      \"source\": \"0001-01-01T00:00:00\",\n" +
                "      \"insert\": \"0001-01-01T00:00:00\",\n" +
                "      \"read\": \"2018-04-16T10:06:59.079Z\",\n" +
                "      \"sent\": \"2018-04-16T10:07:02Z\",\n" +
                "      \"received\": \"0001-01-01T00:00:00\"\n" +
                "    }\n" +
                "  }\n", ArrivalPrediction.class);
        assertEquals("Elephant & Castle Underground Station", a.getDestinationName());
        assertEquals(Instant.ofEpochSecond(500), a.getExpectedArrival().toInstant());

    }

    private ObjectMapper getObjectMapper() {
        return new ObjectMapper()
                .registerModule(new JavaTimeModule());

    }

}